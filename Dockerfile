FROM java:7
MAINTAINER pdhandap@starbucks.com
COPY HelloWorld.java /opt/
RUN apt-get update
RUN javac /opt/HelloWorld.java
WORKDIR /opt
CMD ["java", "HelloWorld"]


